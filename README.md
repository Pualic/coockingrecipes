
## Features

- Laravel 10
- MySQL as the database
- Blade templating
- Bootstrap

## Requirements

- PHP 8.1 or higher.
- Composer
- Node.js & npm
- MySQL

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/Pualic/cookingrecipes.git
   cd cookingrecipes
   ```
2. Install PHP dependencies:
    ```bash
    composer install
   ```
3. Install JavaScript dependencies:
   ```bash
    npm install
   ```
4. Set up the environment:
   ```bash
    cp .env.example .env
   ```
5. Open the .env file and update the following values:
    - DB_DATABASE: your database name
    - DB_USERNAME: your database username
    - DB_PASSWORD: your database password
6. Generate an application key:
   ```bash
    php artisan key:generate
   ```
7. Run database migrations:
    ```bash
    php artisan migrate
   ```
8. Compile assets:
    ```bash
    php artisan db:seed
   ```
9. Link the storage folder:
    ```bash
    php artisan storage:link
   ```
10. Create folder for storing recipe images:
    ```bash
    mkdir -p public/recipes
    ```
11. Compile assets:
    ```bash
    npm run dev
    ```
12. Run the development server:
    ```bash
    php artisan serve
    ```
