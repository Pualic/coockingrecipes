<?php

namespace App\Repositories;

use App\Models\Recipe;

interface RecipeRepositoryInterface
{
    public function create(array $data): Recipe;
    public function update(Recipe $recipe, array $data): bool;
    public function find(int $id): ?Recipe;
    public function delete(Recipe $recipe): ?bool;
}
