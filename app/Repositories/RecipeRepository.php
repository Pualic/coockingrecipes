<?php

namespace App\Repositories;


use App\Models\Recipe;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

class RecipeRepository implements RecipeRepositoryInterface
{
    /**
     * @param array $data
     * @return Recipe
     */
    public function create(array $data): Recipe
    {
        return Recipe::create($data);
    }

    public function update(Recipe $recipe, array $data): bool
    {
        return $recipe->update($data);
    }

    public function find(int $id): ?Recipe
    {
        return Recipe::find($id);
    }

    public function delete(Recipe $recipe): ?bool
    {
        return $recipe->delete();
    }

    public function getLatestRecipes(int $paginate): LengthAwarePaginator
    {
        return Recipe::latest()->paginate($paginate);
    }

    public function getUserRecipes(): ?Collection
    {
        return auth()->user()->recipes()->get();
    }
}
