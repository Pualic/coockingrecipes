<?php

namespace App\Repositories;

use App\Models\CookingStep;
use App\Models\Recipe;
use App\Services\RequestItemCounterService;
use Illuminate\Http\Request;

class CookingStepsRepository
{
    public function __construct(public RequestItemCounterService $counterService)
    {
    }

    public function create($recipe, $request): void
    {
        $cookingSteps = [];

        $numberOfSteps = $this->counterService->countItems($request, 'cookingStep');

        for ($i = 1; $i <= $numberOfSteps; $i++) {
            $cookingStepKey = "cookingStep{$i}";

            $cookingSteps[] = new CookingStep([
                'text' => $request->input($cookingStepKey),
            ]);
        }

        $recipe->cookingSteps()->saveMany($cookingSteps);
    }

    public function update(Request $request, Recipe $recipe): void
    {
        $cookingStepsData = $request->input('cooking_steps', []);
        foreach ($cookingStepsData as $cookingStepData) {
            CookingStep::updateOrCreate(
                ['id' => $cookingStepData['id']],
                [
                    'recipe_id' => $recipe->id,
                    'text' => $cookingStepData['text'],
                ]
            );
        }
    }
}
