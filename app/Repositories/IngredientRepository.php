<?php

namespace App\Repositories;

use App\Models\Ingredient;
use App\Models\Recipe;
use App\Services\RequestItemCounterService;
use Illuminate\Http\Request;

class IngredientRepository
{
    public function __construct(public RequestItemCounterService $counterService)
    {
    }

    public function create(Recipe $recipe,Request $request): void
    {
        $ingredients = [];

        $numberOfIngredients = $this->counterService->countItems($request, 'ingredient');

        for ($i = 1; $i <= $numberOfIngredients; $i++) {
            $ingredientKey = "ingredient{$i}";
            $amountKey = "amount{$i}";

            $ingredients[] = new Ingredient([
                'ingredient' => $request->input($ingredientKey),
                'amount' => $request->input($amountKey),
            ]);
        }

        $recipe->ingredients()->saveMany($ingredients);
    }

    public function update(Request $request, Recipe $recipe): void
    {
        $ingredientsData = $request->input('ingredients', []);
        foreach ($ingredientsData as $ingredientData) {
            Ingredient::updateOrCreate(
                ['id' => $ingredientData['id']],
                [
                    'recipe_id' => $recipe->id,
                    'ingredient' => $ingredientData['ingredient'],
                    'amount' => $ingredientData['amount'],
                ]
            );
        }
    }
}
