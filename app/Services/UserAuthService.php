<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\ValidationException;

class UserAuthService
{

    public function __construct(private UserRepository $userRepository)
    {}

    public function storeUser(array $formFields): User
    {
        if (array_key_exists('image', $formFields) && request()->hasFile('image')) {
            $formFields['image'] = $this->uploadUserImage(request()->file('image'));
        }

        $formFields['password'] = bcrypt($formFields['password']);

        return $this->userRepository->create($formFields);
    }

    private function uploadUserImage(UploadedFile $image): string
    {
        return $image->store('users_avatar', 'public');
    }

    public function checkUserCredentials(array $credentials): RedirectResponse
    {
        if (auth()->attempt($credentials)) {
            request()->session()->regenerate();

            return redirect('/')->with('message', 'You are logged in');
        }

        throw ValidationException::withMessages(['email' => 'Invalid credentials'])
            ->errorBag('login');
    }
}
