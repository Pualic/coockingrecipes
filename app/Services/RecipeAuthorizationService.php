<?php

namespace App\Services;

use App\Models\Recipe;

class RecipeAuthorizationService
{
    public function authorizeRecipeOwner(Recipe $recipe): void
    {
        if ($recipe->user_id != auth()->id()) {
            abort(403, 'Unauthorized Action');
        }
    }
}
