<?php

namespace App\Services;

use Illuminate\Http\Request;

class RequestItemCounterService
{
    public function countItems(Request $request, string $prefix): int
    {
        return collect($request->all())->filter(function ($value, $key) use ($prefix) {
            return str_starts_with($key, $prefix);
        })->count();
    }
}
