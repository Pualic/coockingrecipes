<?php

namespace App\Services;

use App\Models\Recipe;
use App\Repositories\CookingStepsRepository;
use App\Repositories\IngredientRepository;
use App\Repositories\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecipeService
{
    public function __construct(
        public RecipeRepositoryInterface $recipeRepository,
        public IngredientRepository $ingredientRepository,
        public CookingStepsRepository $cookingStepsRepository,
        public ImageRecipeService $imageRecipeService,
        public RecipeAuthorizationService $authorizationService,
    ){}

    public function store(Request $request): void
    {
        DB::transaction(function () use ($request)
        {
            $recipeFields = $request->only(['title', 'meal_type', 'cooking_time', 'serving_size', 'description']);

            if ($request->hasFile('image')) {
                $recipeFields['image'] = $this->imageRecipeService->storeImage($request->file('image'), 'recipes');
            }

            $recipeFields['user_id'] = auth()->id();
            $recipe = $this->recipeRepository->create($recipeFields);

            $this->ingredientRepository->create($recipe, $request);
            $this->cookingStepsRepository->create($recipe, $request);
        });
    }

    public function update(Request $request, Recipe $recipe): void
    {
        DB::transaction(function () use ($request, $recipe)
        {
            $this->authorizationService->authorizeRecipeOwner($recipe);

            $recipeFields = $request->only(['title', 'meal_type', 'cooking_time', 'serving_size', 'description']);

            if ($request->hasFile('image')) {
                $recipeFields['image'] = $this->imageRecipeService->updateImage($request->file('image'), 'recipes', $recipe->image);
            }

            $this->recipeRepository->update($recipe, $recipeFields);

            $this->ingredientRepository->update($request, $recipe);
            $this->cookingStepsRepository->update($request, $recipe);
        });
    }

    public function delete(Recipe $recipe): void
    {
        $this->authorizationService->authorizeRecipeOwner($recipe);
        $this->recipeRepository->delete($recipe);
    }
}
