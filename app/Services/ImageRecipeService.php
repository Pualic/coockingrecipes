<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ImageRecipeService
{
    public function storeImage(UploadedFile $file, string $path): string
    {
        $imageName = $file->getClientOriginalName();
        $file->storeAs($path, $imageName, 'public');
        return $imageName;
    }

    public function updateImage(UploadedFile $file, string $path, ?string $currentImage): string
    {
        if ($currentImage) {
            Storage::disk('public')->delete("{$path}/{$currentImage}");
        }
        return $this->storeImage($file, $path);
    }
}
