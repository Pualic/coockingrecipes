<?php

namespace App\Providers;

use App\Repositories\CookingStepsRepository;
use App\Repositories\IngredientRepository;
use App\Repositories\RecipeRepository;
use App\Repositories\RecipeRepositoryInterface;
use App\Services\RequestItemCounterService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(RecipeRepositoryInterface::class, RecipeRepository::class);

        $this->app->singleton(IngredientRepository::class, function ($app) {
            return new IngredientRepository($app->make(RequestItemCounterService::class));
        });

        $this->app->singleton(CookingStepsRepository::class, function ($app) {
            return new CookingStepsRepository($app->make(RequestItemCounterService::class));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
