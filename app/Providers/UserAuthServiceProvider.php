<?php

namespace App\Providers;

use App\Repositories\UserRepository;
use App\Services\UserAuthService;
use Illuminate\Support\ServiceProvider;

class UserAuthServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(UserAuthService::class, function ($app) {
            return new UserAuthService($app->make(UserRepository::class));
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
