<?php

namespace App\Providers;

use App\Repositories\CookingStepsRepository;
use App\Repositories\IngredientRepository;
use App\Repositories\RecipeRepositoryInterface;
use App\Services\ImageRecipeService;
use App\Services\RecipeAuthorizationService;
use App\Services\RecipeService;
use App\Services\RequestItemCounterService;
use Illuminate\Support\ServiceProvider;

class ServiceServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->singleton(RecipeService::class, function ($app) {
            return new RecipeService(
                $app->make(RecipeRepositoryInterface::class),
                $app->make(IngredientRepository::class),
                $app->make(CookingStepsRepository::class),
                $app->make(ImageRecipeService::class),
                $app->make(RecipeAuthorizationService::class),
            );
        });
        $this->app->singleton(ImageRecipeService::class, function ($app) {
            return new ImageRecipeService();
        });

        $this->app->singleton(RecipeAuthorizationService::class, function ($app) {
            return new RecipeAuthorizationService();
        });

        $this->app->singleton(RequestItemCounterService::class, function ($app) {
            return new RequestItemCounterService();
        });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
