<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRecipeRequest;
use App\Models\Recipe;
use App\Repositories\RecipeRepository;
use App\Services\RecipeService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class RecipeController extends Controller
{

    public function __construct(
        private RecipeService $recipeService,
        private RecipeRepository $recipeRepository,
    )
    {}

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $recipes = $this->recipeRepository->getLatestRecipes(9);

        return view('index', [
            'recipes' => $recipes,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('recipes.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRecipeRequest $request): RedirectResponse
    {
        $this->recipeService->store($request);
        return redirect()->route('home')->with('message', 'Recipe created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Recipe $recipe): View
    {
        return view('recipes.show',[
            'recipe' => $recipe
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Recipe $recipe): View
    {
        return view('recipes.edit', [
            'recipe' => $recipe
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreRecipeRequest $request, Recipe $recipe)
    {
        $this->recipeService->update($request, $recipe);
        return redirect()->route('manage')->with('message', 'Recipe updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Recipe $recipe): RedirectResponse
    {
        $this->recipeService->delete($recipe);
        return redirect()->route('manage')->with('message', 'Listing deleted successfully');
    }

    public function manage(): View
    {
        $recipes = $this->recipeRepository->getUserRecipes();

        return view('recipes.manage',[
            'recipes' => $recipes,
        ]);
    }
}
