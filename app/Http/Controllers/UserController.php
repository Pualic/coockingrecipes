<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLoginUserRequest;
use App\Http\Requests\StoreRegisterUserRequest;
use App\Services\UserAuthService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct(public UserAuthService $userAuthService)
    {}

    public function create(): View
    {
        return view('user.register');
    }

    public function store(StoreRegisterUserRequest $request): RedirectResponse
    {
        $formFields = $request->validated();
        $user = $this->userAuthService->storeUser($formFields);

        auth()->login($user);

        return redirect()->route('home')->with('message', 'User created and logged in');
    }

    public function logout(Request $request): RedirectResponse
    {
        auth()->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->route('home')->with('message', 'You have been logged out');
    }

    public function login(): View
    {
        return view('user.login');
    }

    public function authenticate(StoreLoginUserRequest $request): RedirectResponse
    {
        $credentials = $request->except('_token');

        try {
            return $this->userAuthService->checkUserCredentials($credentials);
        } catch (ValidationException $e) {
            return back()->withErrors($e->errors())->onlyInput('email');
        }
    }
}
