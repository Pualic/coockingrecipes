<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecipeRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'title' => 'required',
            'meal_type' => 'required|not_in:Meal type',
            'cooking_time' => 'required|integer',
            'serving_size' => 'required|integer',
            'description' => 'required|max:255',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:10240',

            'ingredient.*' => 'required|string|max:255',
            'amount.*' => 'required|integer',

            'cookingStep.*' => 'required|string|not_in:null',
        ];
    }

    public function messages(): array
    {
        return [
            'meal_type.not_in' => 'Please select a valid meal type.',
            'description.max' => 'Maximum number of characters is 255'
        ];
    }
}
