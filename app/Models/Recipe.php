<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Recipe extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'meal_type',
        'cooking_time',
        'serving_size',
        'description',
        'image'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function ingredients():HasMany
    {
        return $this->hasMany(Ingredient::class, 'recipe_id');
    }

    public function cookingSteps():HasMany
    {
        return $this->hasMany(CookingStep::class, 'recipe_id');
    }
}
