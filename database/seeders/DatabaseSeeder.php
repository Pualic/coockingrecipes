<?php

namespace Database\Seeders;

use App\Models\Recipe;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory()->create([
            'username' => 'milan',
            'email' => 'milan@gmail.com',
            'password' => 'Milan1'
        ]);

        Recipe::factory()
            ->hasIngredients()
            ->hasCookingSteps()
            ->create([
            'user_id' => 1,
            'title' => 'spaghetti',
            'meal_type' => 'launch',
            'cooking_time' => 60,
            'serving_size' => 2,
            'description' => 'fine i ukusne spagete za rucak',
            'image' => null
        ]);

        User::factory(2)->create();
        Recipe::factory(10)
            ->hasIngredients()
            ->hasCookingSteps()
            ->create();
    }
}
