<?php

namespace Database\Factories;

use App\Models\CookingStep;
use App\Models\Ingredient;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Recipe>
 */
class RecipeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'user_id' => User::factory(),
            'title' => $this->faker->title(),
            'meal_type' => $this->faker->randomElement(['breakfast', 'launch', 'dinner', 'desert']),
            'cooking_time' => $this->faker->randomElement([30,45,60,90,120]),
            'serving_size' => $this->faker->randomElement([1,2,3,4,5,6]),
            'description' => $this->faker->paragraph(2),
            'image' => null,
        ];
    }

    public function hasIngredients(): Factory
    {
        return $this->has(Ingredient::factory()->count(2));
    }

    public function hasCookingSteps(): Factory
    {
        return $this->has(CookingStep::factory()->count(3));
    }
}
