<x-base>
    <h3 class="text-center">Create recipe</h3>
    <div class="offset-3 col-6">

        <form action="{{ route('recipe.store') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="title" id="title" placeholder="name@example.com" value="{{old('title')}}">
                <label for="title">Title</label>
                @error('title')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <select class="form-select form-select mb-3" name="meal_type" id="meal_type" aria-label="Large select example">
                <option selected>Meal type</option>
                <option value="breakfast">Breakfast</option>
                <option value="launch">Launch</option>
                <option value="dinner">Dinner</option>
                <option value="desert">Desert</option>
            </select>
            @error('meal_type')
                <p class="text-danger">{{$message}}</p>
            @enderror

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="cooking_time" id="cooking_time" placeholder="name@example.com" value="{{old('cooking_time')}}">
                <label for="cooking_time">Cooking time (minutes)</label>
                @error('cooking_time')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="serving_size" id="serving_size" placeholder="name@example.com" value="{{old('serving_size')}}">
                <label for="serving_size">Serving Size</label>
                @error('serving_size')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-floating mb-3">
                <textarea class="form-control" name="description" id="description" style="height: 100px; resize: none" placeholder="name@example.com">{{old('description')}}</textarea>
                <label for="description">Short description</label>
                @error('description')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="input-group mb-3">
                <input type="file" name="image" class="form-control" id="image">
                <label class="input-group-text" for="image">Upload</label><br>
                @error('image')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div x-data="{ ingredientCounter: 1 }">
                <h4>Ingredients</h4>

                <template x-for="ingredient in ingredientCounter" :key="ingredient">
                    <div class="input-group mb-3">
                        <span class="input-group-text" x-text="ingredient"></span>
                        <input type="text" class="form-control" :name="'ingredient' + ingredient" aria-label="Ingredient" placeholder="Ingredient">
                        <input type="text" class="form-control" :name="'amount' + ingredient" aria-label="Amount" placeholder="Amount">
                    </div>
                </template>
                <button type="button" @click="ingredientCounter++" class="btn btn-primary mb-3">Add Ingredient</button>
            </div>

            <div x-data="{ cookingStepCounter: 1 }">
                <h4>Cooking steps</h4>

                <template x-for="cookingStep in cookingStepCounter" :key="cookingStep">
                    <div class="input-group mb-3">
                        <span class="input-group-text" x-text="cookingStep"></span>
                        <textarea class="form-control" :name="'cookingStep' + cookingStep" aria-label="With textarea"></textarea>
                    </div>
                </template>
                <button type="button" @click="cookingStepCounter++" class="btn btn-primary mb-3">Add cookingStep</button>
            </div>

            <button class="btn btn-primary mb-3">Submit</button>
        </form>

    </div>
</x-base>
