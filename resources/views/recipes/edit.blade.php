<x-base>
    <h3 class="text-center">Update recipe</h3>
    <div class="offset-3 col-6">

        <form action="{{ route('recipe.update',['recipe' => $recipe->id]) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="title" id="title" value="{{$recipe->title}}">
                <label for="title">Title</label>
                @error('title')
                    <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <select class="form-select form-select mb-3" name="meal_type" id="meal_type" aria-label="Large select example">
                <option value="" disabled>Select Meal Type</option>
                <option value="breakfast" {{ old('meal_type', $recipe->meal_type) === 'breakfast' ? 'selected' : '' }}>Breakfast</option>
                <option value="launch" {{ old('meal_type', $recipe->meal_type) === 'launch' ? 'selected' : '' }}>Launch</option>
                <option value="dinner" {{ old('meal_type', $recipe->meal_type) === 'dinner' ? 'selected' : '' }}>Dinner</option>
                <option value="dessert" {{ old('meal_type', $recipe->meal_type) === 'dessert' ? 'selected' : '' }}>Dessert</option>
            </select>

            @error('meal_type')
            <p class="text-danger">{{$message}}</p>
            @enderror

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="cooking_time" id="cooking_time" value="{{$recipe->cooking_time}}">
                <label for="cooking_time">Cooking time (minutes)</label>
                @error('cooking_time')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-floating mb-3">
                <input type="text" class="form-control" name="serving_size" id="serving_size" placeholder="name@example.com" value="{{$recipe->serving_size}}">
                <label for="serving_size">Serving Size</label>
                @error('serving_size')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="form-floating mb-3">
                <textarea class="form-control" name="description" id="description" style="height: 100px; resize: none" placeholder="name@example.com">{{$recipe->description}}</textarea>
                <label for="description">Short description</label>
                @error('description')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <div class="input-group mb-3">
                <input type="file" name="image" class="form-control" id="image">
                <label class="input-group-text" for="image">Upload</label><br>
            </div>
            <div>
                <img
                    src="{{$recipe->image ? asset('storage/recipes/'. $recipe->image) : asset('/images/no-recipe-image.jpg')}}"
                    style="max-height: 100px; max-width: 100px"
                    alt=""
                />
                @error('image')
                <p class="text-danger">{{$message}}</p>
                @enderror
            </div>

            <h4>Ingredients</h4>
            <div>
                @foreach($recipe->ingredients as $ingredient)
                    <div class="input-group mb-3">
                        <span class="input-group-text"></span>
                        <input type="hidden" name="ingredients[{{ $ingredient->id }}][id]" value="{{ $ingredient->id }}">
                        <input type="text" class="form-control" name="ingredients[{{ $ingredient->id }}][ingredient]" aria-label="Ingredient" placeholder="Ingredient" value="{{ $ingredient->ingredient }}">
                        <input type="text" class="form-control" name="ingredients[{{ $ingredient->id }}][amount]" aria-label="Amount" placeholder="Amount" value="{{ $ingredient->amount }}">
                    </div>
                @endforeach
            </div>

            <h4>Cooking steps</h4>
            <div>
                @foreach($recipe->cookingSteps as $cookingStep)
                    <div class="input-group mb-3">
                        <span class="input-group-text"></span>
                        <input type="hidden" name="cooking_steps[{{ $cookingStep->id }}][id]" value="{{ $cookingStep->id }}">
                        <textarea class="form-control" name="cooking_steps[{{ $cookingStep->id }}][text]" aria-label="With textarea">{{ $cookingStep->text }}</textarea>
                    </div>
                @endforeach
            </div>

            <button class="btn btn-primary">Update recipe</button>
        </form>
    </div>
</x-base>
