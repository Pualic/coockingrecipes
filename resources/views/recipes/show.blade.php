<x-base>
    <div>
        <div class="text-center">
            <img src="{{$recipe->image ? asset('storage/recipes/'. $recipe->image) : asset('/images/no-recipe-image.jpg')}}"
                 style="max-height: 250px; max-width: 250px"  alt="">
            <h2>{{$recipe->title}} </h2>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Meal</th>
                    <th scope="col">Cooking time</th>
                    <th scope="col">Serving size</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{$recipe->meal_type}}</td>
                    <td>{{$recipe->cooking_time}}</td>
                    <td>{{$recipe->serving_size}}</td>
                </tr>
                </tbody>
            </table>

            <h2>Ingredients</h2>

            <table class="table">
                <thead>
                <tr>
                    <th>Ingredient</th>
                    <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                @foreach($recipe->ingredients as $ingredient)
                    <tr>
                        <td>{{$ingredient->ingredient}}</td>
                        <td>{{$ingredient->amount}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="mt-5">
            <h2>Description:</h2>
            <p>{{$recipe->description}}</p>
        </div>

        <div class="mt-5">
            <h2>Steps:</h2>
            @php
                $stepOrder = 1
            @endphp
            @foreach($recipe->cookingSteps as $cookingStep)
                <div class="row my-4">
                    <div class="col-2 text-center">
                        <h4>Step {{$stepOrder}}:</h4>
                    </div>
                    <div class="col">
                        {{$cookingStep->text}}
                    </div>
                </div>
            @php
                $stepOrder++
            @endphp
            @endforeach
        </div>
    </div>
</x-base>
