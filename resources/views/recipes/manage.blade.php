<x-base>
    <table class="table table-striped">
        <thead class="table-primary">
        <tr>
            <th scope="col">Title</th>
            <th scope="col">Meal type</th>
            <th scope="col">Cooking time</th>
            <th scope="col">Serving size</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($recipes as $recipe)
            <tr>
                <td>{{$recipe->title}}</td>
                <td>{{$recipe->meal_type}}</td>
                <td>{{$recipe->cooking_time}}</td>
                <td>{{$recipe->serving_size}}</td>
                <td class="col-3">
                    <a href="{{ route('recipe.show', ['recipe' => $recipe->id]) }}" class="btn btn-primary">Check recipe</a>
                    <a href="{{route('recipe.edit', ['recipe' => $recipe->id])}}" style="text-decoration: none">
                        <button class="btn btn-info d-inline">Edit</button>
                    </a>
                    <form action="{{route('recipe.destroy', ['recipe' => $recipe->id])}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</x-base>
