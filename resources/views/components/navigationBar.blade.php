<nav class="navbar bg-primary navbar-expand-lg">
    <div class="container">
        <a class="navbar-brand d-flex align-items-center text-white" href="{{route('home')}}">
            <i class="bi bi-house-door-fill fs-3"></i>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active text-white" aria-current="page" href="{{route('home')}}">Home</a>
                </li>
                @auth()
                    <li>
                        <a class="nav-link active text-white" href="{{route('recipe.create')}}">Create recipe</a>
                    </li>
                    <li>
                        <a class="nav-link active text-white" href="{{route('manage')}}">My recipes</a>
                    </li>
                @endauth
            </ul>
            @auth()
                <div class="d-flex align-items-center">
                    <h4 class="text-white mb-0 me-3 ">Welcome {{ auth()->user()->username }}</h4>
                    <a href="{{ route('logout') }}">
                        <button class="btn btn-outline-light">Logout</button>
                    </a>
                </div>
            @else
                <div class="p-2">
                    <a href="{{route('login')}}">
                        <button class="btn btn-outline-light">login</button>
                    </a>
                    <a href="{{route('register')}}">
                        <button class="btn btn-outline-light">register</button>
                    </a>
                </div>
            @endauth
        </div>
    </div>
</nav>
