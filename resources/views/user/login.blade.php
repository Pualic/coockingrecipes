<x-base>
    <section class="vh-100 bg-image"
             style="background-image: url({{asset('images/background_image/register-form.jpg')}});">
        <div class="mask d-flex align-items-center h-100 gradient-custom-3">
            <div class="container h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                        <div class="card" style="border-radius: 15px;">
                            <div class="card-body p-5">
                                <h2 class="text-uppercase text-center mb-5">Log into your account</h2>

                                <form action="{{route('user.authenticate')}}" method="POST">
                                    @csrf
                                    <div class="form-outline mb-4">
                                        <input type="email" name="email" id="email" class="form-control form-control-lg" value="{{old('email')}}"/>
                                        <label class="form-label" for="email">Email</label>
                                        @error('email')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-outline mb-4">
                                        <input type="password" name="password" id="password" class="form-control form-control-lg" value="{{old('password')}}"/>
                                        <label class="form-label" for="password">Password</label>
                                        @error('password')
                                        <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="d-flex justify-content-center">
                                        <button class="btn btn-primary btn-block btn-lg gradient-custom-4">Log in</button>
                                    </div>

                                    <p class="text-center text-muted mt-5 mb-0">Don't have an account?
                                        <a href="{{route('register')}}" class="fw-bold text-body"><u>Register here</u></a>
                                    </p>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-base>
