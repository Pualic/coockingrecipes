<x-base>
    <section class="vh-100 bg-image"
             style="background-image: url({{asset('images/background_image/register-form.jpg')}});">
        <div class="mask d-flex align-items-center h-100 gradient-custom-3">
            <div class="container h-100">
                <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                        <div class="card" style="border-radius: 15px;">
                            <div class="card-body p-5">
                                <h2 class="text-uppercase text-center mb-5">Create an account</h2>

                                <form action="{{route('user.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-outline mb-4">
                                        <input type="text" name="username" id="username" class="form-control form-control-lg" value="{{old('username')}}" />
                                        <label class="form-label" for="username">Username</label>
                                        @error('username')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-outline mb-4">
                                        <input type="email" name="email" id="email" class="form-control form-control-lg" value="{{old('email')}}"/>
                                        <label class="form-label" for="email">Email</label>
                                        @error('email')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-outline mb-4">
                                        <input type="password" name="password" id="password" class="form-control form-control-lg" value="{{old('password')}}"/>
                                        <label class="form-label" for="password">Password</label>
                                        @error('password')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="form-outline mb-4">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control form-control-lg" value="{{old('password_confirmation')}}"/>
                                        <label class="form-label" for="password_confirmation">Repeat your password</label>
                                        @error('password_confirmation')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="input-group mb-3">
                                        <input type="file" name="image" class="form-control" id="image">
                                        <label class="input-group-text" for="image">Upload</label>
                                        @error('image')
                                            <p class="text-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                    <div class="d-flex justify-content-center">
                                        <button class="btn btn-primary btn-block btn-lg gradient-custom-4">Register</button>
                                    </div>

                                    <p class="text-center text-muted mt-5 mb-0">Have already an account?
                                        <a href="{{route('login')}}" class="fw-bold text-body"><u>Login here</u></a>
                                    </p>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-base>
