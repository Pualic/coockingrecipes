<x-base>
    <div class="row row-cols-1 row-cols-md-3 g-4">
        @foreach($recipes as $recipe)
            <div class="col">
                <div class="card tex" style="height: 450px; display: flex; flex-direction: column;">
                    <div class="text-center" style="height: 200px; overflow: hidden;">
                        <img src="{{$recipe->image ? asset('storage/recipes/'. $recipe->image) : asset('/images/no-recipe-image.jpg')}}"
                             class="card-img-top"
                             style="max-height: 100%; object-fit: cover;"
                             alt="Recipe image">
                    </div>
                    <div class="card-body flex-grow-1">
                        <h5 class="card-title">{{$recipe->title}}</h5>
                        <p class="card-text">{{$recipe->description}}</p>
                    </div>
                    <div class="d-flex justify-content-between text-center p-3">
                        <div class="pt-2">
                            <p>Author: {{$recipe->user->username}}</p>
                        </div>
                        <div class="ms-auto">
                            <a href="{{ route('recipe.show', ['recipe' => $recipe->id]) }}" class="btn btn-primary">Check recipe</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <div class="d-flex justify-content-end mt-2">
        {!! $recipes->links() !!}
    </div>
</x-base>
