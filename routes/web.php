<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecipeController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [RecipeController::class, 'index'])->name('home');

Route::get('/recipe/manage', [RecipeController::class, 'manage'])->name('manage')->middleware('auth');
Route::resource('/recipe', RecipeController::class)->except(['index'])->middleware('auth')->only(['create', 'store', 'show', 'edit', 'update', 'destroy']);

//user authentication
Route::get('/register', [UserController::class, 'create'])->name('register')->middleware('guest');
Route::post('/users', [UserController::class, 'store'])->name('user.store');
Route::get('/login', [UserController::class, 'login'])->name('login')->middleware('guest');
Route::post('/users/authenticate', [UserController::class, 'authenticate'])->name('user.authenticate');
Route::get('/logout', [UserController::class, 'logout'])->name('logout')->middleware('auth');
